<?php
$arUrlRewrite=array (
  0 => 
  array (
    'CONDITION' => '#^/api/item/([\\d]+)/($|\\?.+$)$#',
    'RULE' => 'component=api.item&params[id]=$1',
    'ID' => 'democontent.news:api.item',
    'PATH' => '/local/democontent2.news/prolog.php',
    'SORT' => 1,
  ),
  1 => 
  array (
    'CONDITION' => '#^/api/(categories|items)/($|\\?.+$)$#',
    'RULE' => 'component=api.$1',
    'ID' => 'democontent.news:api.$1',
    'PATH' => '/local/democontent2.news/prolog.php',
    'SORT' => 2,
  ),
  2 => 
  array (
    'CONDITION' => '#^/(turbo|zen)/($|\\?.+$)$#',
    'RULE' => 'component=$1',
    'ID' => 'democontent.news:$1',
    'PATH' => '/local/democontent2.news/prolog.php',
    'SORT' => 3,
  ),
  3 => 
  array (
    'CONDITION' => '#^/amp/([\\d]+)/($|\\?.+$)$#',
    'RULE' => 'component=amp.detail&params[id]=$1',
    'ID' => 'democontent.news:amp',
    'PATH' => '/local/democontent2.news/prolog.php',
    'SORT' => 4,
  ),
  4 => 
  array (
    'CONDITION' => '#^/author-([\\d]+)/($|\\?.+$)$#',
    'RULE' => 'component=author&params[id]=$1',
    'ID' => 'democontent.news:author',
    'PATH' => '/local/democontent2.news/index.php',
    'SORT' => 5,
  ),
  5 => 
  array (
    'CONDITION' => '#^/tag-([a-z-]+)/($|\\?.+$)$#',
    'RULE' => 'component=tag.list&params[tag]=$1',
    'ID' => 'democontent.news:tag.list',
    'PATH' => '/local/democontent2.news/index.php',
    'SORT' => 6,
  ),
  6 => 
  array (
    'CONDITION' => '#^/([a-z_-]+)/([\\d]{4}+)/([\\d]{2}+)/([\\d]{2}+)/([\\da-z_-]+)/($|\\?.+$)$#',
    'RULE' => 'component=detail&params[sectionCode]=$1&params[year]=$2&params[month]=$3&params[day]=$4&params[itemCode]=$5',
    'ID' => 'democontent.news:detail',
    'PATH' => '/local/democontent2.news/index.php',
    'SORT' => 7,
  ),
  7 => 
  array (
    'CONDITION' => '#^/([a-z_-]+)/([\\d]{4}+)/([\\d]{2}+)/([\\d]{2}+)/($|\\?.+$)$#',
    'RULE' => 'component=list&params[sectionCode]=$1&params[year]=$2&params[month]=$3&params[day]=$4',
    'ID' => 'democontent.news:list',
    'PATH' => '/local/democontent2.news/index.php',
    'SORT' => 8,
  ),
  8 => 
  array (
    'CONDITION' => '#^/([a-z_-]+)/([\\d]{4}+)/([\\d]{2}+)/($|\\?.+$)$#',
    'RULE' => 'component=list&params[sectionCode]=$1&params[year]=$2&params[month]=$3',
    'ID' => 'democontent.news:list',
    'PATH' => '/local/democontent2.news/index.php',
    'SORT' => 9,
  ),
  9 => 
  array (
    'CONDITION' => '#^/([a-z_-]+)/([\\d]{4}+)/($|\\?.+$)$#',
    'RULE' => 'component=list&params[sectionCode]=$1&params[year]=$2',
    'ID' => 'democontent.news:list',
    'PATH' => '/local/democontent2.news/index.php',
    'SORT' => 10,
  ),
  10 => 
  array (
    'CONDITION' => '#^/([a-z_-]+)/($|\\?.+$)$#',
    'RULE' => 'component=list&params[sectionCode]=$1',
    'ID' => 'democontent.news:list',
    'PATH' => '/local/democontent2.news/index.php',
    'SORT' => 11,
  ),
);
