<?
use Bitrix\Iblock\ElementTable;
AddEventHandler("iblock", "OnAfterIBlockElementAdd",  Array("MyClass", "OnAfterIBlockElementAddHandler"));

class MyClass
{
    // создаем обработчик события "OnAfterIBlockElementAdd"
    function OnAfterIBlockElementAddHandler(&$arFields)
    {
        if ($arFields['IBLOCK_ID'] === 13) {
            CEvent::SendImmediate(
                "NEW_REVIEW",
                "s2",
                [
                    'REVIEW_LINK' => SITE_SERVER_NAME.'/bitrix/admin/iblock_element_edit.php?IBLOCK_ID='.$arFields['IBLOCK_ID'].'&type=service&ID='.$arFields['ID'].'&lang=ru&WF=Y&find_section_section=0'
                ]
            );
        }
    }
}

function prt($obj)
{
    global  $USER;
    if($USER->IsAdmin())
    {
      echo "<pre style=\"background-color:#FFF;color:#000;font-size:10px;\"><span style=\"color: red;\">printAdmin:</span>\n";
      print_r($obj);
      echo "</pre>";
    }
}
?>