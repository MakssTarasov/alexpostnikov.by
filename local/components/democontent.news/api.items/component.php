<?php
/**
 * Author: Ruslan Semagin
 * Email: pixel.365.24@gmail.com
 * Skype: pixel365
 * WebSite: semagin.com
 * Date: 01.03.2018
 * Time: 16:27
 */
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
if (\Bitrix\Main\ModuleManager::isModuleInstalled('democontent2.news')) {
    \Bitrix\Main\Loader::includeModule('iblock');

    if (\Bitrix\Main\Loader::includeModule('democontent2.news')) {
        header('Content-Type: application/json');

        $params = [
            'offset' => 0,
            'category' => 0
        ];

        if (isset($_REQUEST['offset'])) {
            if (intval($_REQUEST['offset'])) {
                $params['offset'] = intval($_REQUEST['offset']);
            }
        }

        if (isset($_REQUEST['category'])) {
            if (intval($_REQUEST['category'])) {
                $params['category'] = intval($_REQUEST['category']);
            }
        }

        $obj = new \Democontent2\News\Api\Items(intval($params['offset']), intval($params['category']));
        $result = $obj->get();

        echo \Bitrix\Main\Web\Json::encode(
            [
                'error' => ((count($result) > 0) ? 0 : 1),
                'count' => count($result),
                'result' => $result
            ]
        );

        unset($obj, $result, $params);
    } else {
        echo \Bitrix\Main\Web\Json::encode(
            [
                'error' => 1
            ]
        );
    }
} else {
    echo \Bitrix\Main\Web\Json::encode(
        [
            'error' => 1
        ]
    );
}