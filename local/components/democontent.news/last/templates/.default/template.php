<?php
/**
 * Author: Ruslan Semagin
 * Email: pixel.365.24@gmail.com
 * Skype: pixel365
 * WebSite: semagin.com
 * Date: 24.01.2018
 * Time: 19:05
 */
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

if (method_exists($this, 'setFrameMode')) {
    $this->setFrameMode(true);
}

if (!count($arResult['ITEMS'])) {
    return;
}

\Bitrix\Main\Page\Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . '/js/jquery.timeago.js');
use Bitrix\Main\Localization\Loc;

?>
<div class="title"><?= Loc::getMessage('LAST_NEWS') ?></div>
<div class="row">
    <?php
    foreach ($arResult['ITEMS'] as $k => $v) {
        $arButtons = \CIBlock::GetPanelButtons(
            $v['IBLOCK_ID'],
            $v['ID'],
            0,
            array("SECTION_BUTTONS" => false, "SESSID" => false)
        );

        $arItem["EDIT_LINK"] = $arButtons["edit"]["edit_element"]["ACTION_URL"];
        $arItem["DELETE_LINK"] = $arButtons["edit"]["delete_element"]["ACTION_URL"];

        $this->AddEditAction(
            $v['ID'],
            $arItem['EDIT_LINK'],
            CIBlock::GetArrayByID(
                $v['IBLOCK_ID'],
                "ELEMENT_EDIT"
            )
        );
        $this->AddDeleteAction(
            $v['ID'],
            $arItem['DELETE_LINK'],
            CIBlock::GetArrayByID(
                $v['IBLOCK_ID'],
                "ELEMENT_DELETE"
            ),
            array(
                "CONFIRM" => Loc::getMessage('CONFIRM_DELETE')
            )
        );
        ?>
        <div class="item col-md-12 col-sm-4 col-xs-6" id="<?= $this->GetEditAreaId($v['ID']) ?>">
            <div class="thumbnail thumbnail-article">
                <div class="caption">
                    <div class="txt"><?= $v['NAME'] ?></div>
                    <div class="label label-transparent">
                        <span class="datetime" title="<?= $v['DATE_YMD_HIS'] ?>"></span>
                    </div>
                </div>
                <a class="lnk-abs" href="<?= $v['URL'] ?>"></a>
            </div>
        </div>
    <?php
    }
    ?>
</div>
<script>
    $(document).ready(function () {
        (function () {
            function numpf(n, f, s, t) {
                var n10 = n % 10;
                if ((n10 == 1) && ( (n == 1) || (n > 20) )) {
                    return f;
                } else if ((n10 > 1) && (n10 < 5) && ( (n > 20) || (n < 10) )) {
                    return s;
                } else {
                    return t;
                }
            }

            jQuery.timeago.settings.strings = {
                prefixAgo: null,
                prefixFromNow: "<?=Loc::getMessage('TIME_ACROSS')?>",
                suffixAgo: "<?=Loc::getMessage('TIME_BACK')?>",
                suffixFromNow: null,
                seconds: "<?=Loc::getMessage('TIME_LESS_MINUTE')?>",
                minute: "<?=Loc::getMessage('TIME_MINUTE_4')?>",
                minutes: function (value) {
                    return numpf(value, "%d <?=Loc::getMessage('TIME_MINUTE_1')?>", "%d <?=Loc::getMessage('TIME_MINUTE_2')?>", "%d <?=Loc::getMessage('TIME_MINUTE_3')?>");
                },
                hour: "<?=Loc::getMessage('TIME_HOUR_1')?>",
                hours: function (value) {
                    return numpf(value, "%d <?=Loc::getMessage('TIME_HOUR_1')?>", "%d <?=Loc::getMessage('TIME_HOUR_2')?>", "%d <?=Loc::getMessage('TIME_HOUR_3')?>");
                },
                day: "<?=Loc::getMessage('TIME_DAY_1')?>",
                days: function (value) {
                    return numpf(value, "%d <?=Loc::getMessage('TIME_DAY_1')?>", "%d <?=Loc::getMessage('TIME_DAY_2')?>", "%d <?=Loc::getMessage('TIME_DAY_3')?>");
                },
                month: "<?=Loc::getMessage('TIME_MONTH_1')?>",
                months: function (value) {
                    return numpf(value, "%d <?=Loc::getMessage('TIME_MONTH_1')?>", "%d <?=Loc::getMessage('TIME_MONTH_2')?>", "%d <?=Loc::getMessage('TIME_MONTH_3')?>");
                },
                year: "<?=Loc::getMessage('TIME_YEAR_1')?>",
                years: function (value) {
                    return numpf(value, "%d <?=Loc::getMessage('TIME_YEAR_1')?>", "%d <?=Loc::getMessage('TIME_YEAR_2')?>", "%d <?=Loc::getMessage('TIME_YEAR_3')?>");
                }
            };
        })();

        jQuery(".datetime").timeago();
    });
</script>
