<?php
/**
 * Author: Ruslan Semagin
 * Email: pixel.365.24@gmail.com
 * Skype: pixel365
 * WebSite: semagin.com
 * Date: 30.01.2018
 * Time: 12:43
 */
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Config\Option;

$items = new \Democontent2\News\Items();
$items->setTtl(3600);

$items = $items->getLast(500, true, true);

$sections = \Democontent2\News\Iblock::sections();
$domain = Option::get('democontent2.news', 'yandex_turbo_domain');

header('Content-Type: text/xml; charset=' . SITE_CHARSET, true);
echo '<?xml version="1.0" encoding="' . SITE_CHARSET . '"?>';
?>
<rss xmlns:yandex="http://news.yandex.ru" xmlns:media="http://search.yahoo.com/mrss/"
     xmlns:turbo="http://turbo.yandex.ru" version="2.0">
    <channel>
        <title>
            <?php
            echo \Democontent2\News\Utils::specialCharsReplace(Option::get('democontent2.news', 'yandex_turbo_channel_name'));
            ?>
        </title>
        <link><?= $domain ?></link>
        <description>
            <?php
            echo \Democontent2\News\Utils::specialCharsReplace(Option::get('democontent2.news', 'yandex_turbo_description'));
            ?>
        </description>
        <language>ru</language>
        <?php
        if (Option::get('democontent2.news', 'yandex_turbo_metrika_id')) {
            echo '<yandex:analytics type="Yandex" id="' . Option::get('democontent2.news', 'yandex_turbo_metrika_id') . '"></yandex:analytics>';
        }

        if (Option::get('democontent2.news', 'yandex_turbo_google_analytics_id')) {
            echo '<yandex:analytics type="Google" id="' . Option::get('democontent2.news', 'yandex_turbo_google_analytics_id') . '"></yandex:analytics>';
        }

        if (Option::get('democontent2.news', 'yandex_turbo_mediascope_id')) {
            echo '<yandex:analytics type="Mediascope" id="' . Option::get('democontent2.news', 'yandex_turbo_mediascope_id') . '"></yandex:analytics>';
        }

        if (Option::get('democontent2.news', 'yandex_turbo_rambler_id')) {
            echo '<yandex:analytics type="Rambler" id="' . Option::get('democontent2.news', 'yandex_turbo_rambler_id') . '"></yandex:analytics>';
        }

        if (Option::get('democontent2.news', 'yandex_turbo_mail_id')) {
            echo '<yandex:analytics type="MailRu" id="' . Option::get('democontent2.news', 'yandex_turbo_mail_id') . '"></yandex:analytics>';
        }

        foreach ($items as $k => $v) {
            $imageId = 0;

            if (intval($v['PREVIEW_PICTURE'])) {
                $imageId = intval($v['PREVIEW_PICTURE']);
            } else {
                $imageId = intval($v['DETAIL_PICTURE']);
            }

            $image = CFile::ResizeImageGet(
                $imageId,
                array(
                    'width' => 500,
                    'height' => 500
                ),
                BX_RESIZE_IMAGE_PROPORTIONAL_ALT,
                true
            );

            ?>
            <item turbo="true">
                <title><?= \Democontent2\News\Utils::specialCharsReplace($v['NAME']) ?></title>
                <link><?= $domain . $v['URL'] ?></link>
                <turbo:source><?= $domain . $v['URL'] ?></turbo:source>
                <turbo:topic><?= \Democontent2\News\Utils::specialCharsReplace($v['NAME']) ?></turbo:topic>
                <?php
                if (isset($v['AUTHOR'])) {
                    echo '<author>' . \Democontent2\News\Utils::specialCharsReplace($v['AUTHOR']['NAME'] . ' ' . $v['AUTHOR']['LAST_NAME']) . '</author>';
                }
                ?>
                <category></category>
                <pubDate><?= date(DATE_RFC822, strtotime($v['DATE_CREATE'])); ?></pubDate>
                <turbo:content>
                    <![CDATA[
                    <header>
                        <?php
                        if ($imageId) {
                            ?>
                            <figure>
                                <img src="<?= $domain . $image['src'] ?>"/>
                            </figure>
                        <?php
                        }
                        ?>
                        <h1><?= \Democontent2\News\Utils::specialCharsReplace($v['NAME']) ?></h1>
                    </header>
                    <?php
                    echo $v['DETAIL_TEXT'];

                    if (isset($v['GALLERY'])) {
                        ?>
                        <div data-block="gallery">
                            <header><?= Loc::getMessage('GALLERY') ?></header>
                            <?php
                            foreach ($v['GALLERY'] as $img) {
                                echo '<img src="' . $domain . CFile::GetPath($img) . '"/>';
                            }
                            ?>
                        </div>
                    <?php
                    }
                    if (count($sections) > 0) {
                        ?>
                        <menu>
                            <?php
                            foreach ($sections as $section) {
                                ?>
                                <a href="<?= $domain . '/' . $section['CODE'] ?>/"><?= $section['NAME'] ?></a>
                            <?php
                            }
                            ?>
                        </menu>
                    <?php
                    }
                    ?>
                    <div data-block="share"></div>
                    ]]>
                </turbo:content>
            </item>
        <?php
        }
        ?>
    </channel>
</rss>