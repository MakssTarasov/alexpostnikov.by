<?php
/**
 * Author: Ruslan Semagin
 * Email: pixel.365.24@gmail.com
 * Skype: pixel365
 * WebSite: semagin.com
 * Date: 24.01.2018
 * Time: 14:37
 */
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

$item = new \Democontent2\News\Item($arParams);

$arResult['ITEM'] = $item->get();
$arResult['NAV_CHAIN'] = $item->getNavChain();
$arResult['VIEW_STAT'] = 0;
$arResult['GALLERY'] = [];

if (intval($arResult['ITEM']['ID'])) {
    if (isset($arResult['ITEM']['PROPERTIES']['GALLERY'])) {
        foreach ($arResult['ITEM']['PROPERTIES']['GALLERY'] as $image) {
            if (isset($image['VALUE'])) {
                $img = CFile::ResizeImageGet(
                    $image['VALUE'],
                    [
                        'width' => 860,
                        'height' => 860
                    ],
                    BX_RESIZE_IMAGE_PROPORTIONAL_ALT,
                    true
                );
                $arResult['GALLERY'][] = $img;
            }
        }
    }
    $arResult['VIEW_STAT'] = $item->viewStatistics(intval($arResult['ITEM']['ID']));

    $this->IncludeComponentTemplate();
} else {
    CHTTP::SetStatus("404 Not Found");
    @define("ERROR_404", "Y");
    @define('NEWS_404', $this->__name);
}
