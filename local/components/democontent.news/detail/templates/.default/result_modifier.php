<?php
/**
 * Author: Ruslan Semagin
 * Email: pixel.365.24@gmail.com
 * Skype: pixel365
 * WebSite: semagin.com
 * Date: 02.12.2017
 * Time: 18:24
 */
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

$title = $arResult['ITEM']['NAME'];
$description = substr(strip_tags($arResult['ITEM']['DETAIL_TEXT']), 0, 200);

foreach ($arResult['ITEM']['META'] as $k => $v) {
    switch ($k) {
        case 'ELEMENT_META_TITLE':
            $title = $v;
            break;
        case 'ELEMENT_META_KEYWORDS':
            $APPLICATION->SetPageProperty("keywords", $v);
            break;
        case 'ELEMENT_META_DESCRIPTION':
            $description = $v;
            break;
    }
}

$context = \Bitrix\Main\Application::getInstance()->getContext();

$proto = 'http://';
switch ($context->getServer()->get('HTTP_HTTPS')) {
    case 'YES':
        $proto = 'https://';
        break;
}

$APPLICATION->SetTitle($title);
$APPLICATION->SetPageProperty("title", $title);
$APPLICATION->SetPageProperty("description", $description);

\Bitrix\Main\Page\Asset::getInstance()->addString('<link rel="canonical" href="' . $APPLICATION->GetCurPage(false) . '" />');
\Bitrix\Main\Page\Asset::getInstance()->addString('<meta property="og:type" content="website">');
\Bitrix\Main\Page\Asset::getInstance()->addString('<meta property="og:site_name" content="">');
\Bitrix\Main\Page\Asset::getInstance()->addString('<meta property="og:locale" content="ru_RU">');
\Bitrix\Main\Page\Asset::getInstance()->addString('<meta property="og:title" content="' . $title . '">');
\Bitrix\Main\Page\Asset::getInstance()->addString('<meta property="og:url" content="' . $proto . $context->getServer()->getHttpHost() . $APPLICATION->GetCurPage(false) . '">');

if ($description) {
    \Bitrix\Main\Page\Asset::getInstance()->addString('<meta property="og:description" content="' . mb_substr(strip_tags($description), 0, 160) . '">');
}

if (intval($arResult['ITEM']['DETAIL_PICTURE'])) {
    \Bitrix\Main\Page\Asset::getInstance()->addString(
        '<meta property="og:image" content="' . $proto . $context->getServer()->getHttpHost() . CFile::GetPath(intval($arResult['ITEM']['DETAIL_PICTURE'])) . '">'
    );
}
