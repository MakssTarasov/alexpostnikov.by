<?php
/**
 * Author: Ruslan Semagin
 * Email: pixel.365.24@gmail.com
 * Skype: pixel365
 * WebSite: semagin.com
 * Date: 01.11.2017
 * Time: 11:46
 */
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

$path = array();

foreach ($arResult['NAV_CHAIN'] as $navChain) {
    $path[$navChain['CODE']] = $navChain['CODE'];

    $link = implode('/', $path);

    $APPLICATION->AddChainItem($navChain['NAME'], SITE_DIR . $link . '/');
}

\Bitrix\Main\Page\Asset::getInstance()->addString('<link rel="canonical" href="' . $APPLICATION->GetCurPage(false) . '" />');
\Bitrix\Main\Page\Asset::getInstance()->addString('<link rel="amphtml" href="' . SITE_DIR . 'amp/' . $arResult['ITEM']['ID'] . '/">');
