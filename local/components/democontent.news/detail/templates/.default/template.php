<?php
/**
 * Author: Ruslan Semagin
 * Email: pixel.365.24@gmail.com
 * Skype: pixel365
 * WebSite: semagin.com
 * Date: 24.01.2018
 * Time: 19:05
 */
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

if (method_exists($this, 'setFrameMode')) {
    $this->setFrameMode(true);
}

\Bitrix\Main\Page\Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . '/js/jquery.timeago.js');
\Bitrix\Main\Page\Asset::getInstance()->addJs('//yastatic.net/es5-shims/0.0.2/es5-shims.min.js');
\Bitrix\Main\Page\Asset::getInstance()->addJs('//yastatic.net/share2/share.js');

use Bitrix\Main\Localization\Loc;

$arButtons = \CIBlock::GetPanelButtons(
    $arResult['ITEM']['IBLOCK_ID'],
    $arResult['ITEM']['ID'],
    0,
    array("SECTION_BUTTONS" => false, "SESSID" => false)
);

$arItem["EDIT_LINK"] = $arButtons["edit"]["edit_element"]["ACTION_URL"];
$arItem["DELETE_LINK"] = $arButtons["edit"]["delete_element"]["ACTION_URL"];

$this->AddEditAction(
    $arResult['ITEM']['ID'],
    $arItem['EDIT_LINK'],
    CIBlock::GetArrayByID(
        $arResult['ITEM']['IBLOCK_ID'],
        "ELEMENT_EDIT"
    )
);
$this->AddDeleteAction(
    $arResult['ITEM']['ID'],
    $arItem['DELETE_LINK'],
    CIBlock::GetArrayByID(
        $arResult['ITEM']['IBLOCK_ID'],
        "ELEMENT_DELETE"
    ),
    array(
        "CONFIRM" => Loc::getMessage('CONFIRM_DELETE')
    )
);

?>
<article class="content" id="<?= $this->GetEditAreaId($arResult['ITEM']['ID']) ?>">
    <div class="main-pict<?= ((!isset($arResult['ITEM']['COVER'])) ? ' not-pict' : '') ?>">
        <? if (isset($arResult['ITEM']['COVER'])): ?>
            <? if (intval($arResult['ITEM']['COVER'])): ?>
                <?php
                $cover = CFile::ResizeImageGet(
                    intval($arResult['ITEM']['COVER']),
                    array(
                        'width' => 1170,
                        'height' => 537,
                    ),
                    BX_RESIZE_IMAGE_PROPORTIONAL_ALT,
                    true
                );
                ?>
                <img class="img-responsive" src="<?= $cover['src'] ?>" alt="<?= $arResult['ITEM']['NAME'] ?>">
            <? endif; ?>
        <? endif; ?>

        <div class="inner">
            <div class="inner-content">
                <div class="top">
                    <div class="label label-danger text-uppercase">
                        <?= ((isset($arResult['ITEM']['SECTION_PARAMS']['NAME'])) ? $arResult['ITEM']['SECTION_PARAMS']['NAME'] : '') ?>
                    </div>
                    <span class="datetime"
                          title="<?= date('Y-m-d H:i:s', strtotime($arResult['ITEM']['DATE_CREATE'])) ?>"></span>
                </div>
                <h1><?= $arResult['ITEM']['NAME'] ?></h1>

                <div class="btm">
                    <span><i class="fa fa-eye"></i> <?= $arResult['VIEW_STAT'] ?></span>

                    <? if (isset($arResult['ITEM']['AUTHOR'])): ?>
                        <a href="<?= SITE_DIR ?>author-<?= $arResult['ITEM']['AUTHOR']['ID'] ?>/" class="name">
                            <i class="fa fa-user-o"> </i><?= $arResult['ITEM']['AUTHOR']['NAME'] ?> <?= $arResult['ITEM']['AUTHOR']['LAST_NAME'] ?>
                        </a>
                    <? endif; ?>
                </div>
                <div class="settings">
                    <div class="ya-share2"
                         data-services="vkontakte,facebook,odnoklassniki,moimir,gplus,twitter,viber,whatsapp,skype,telegram"
                         data-counter=""></div>
                </div>
            </div>
        </div>
    </div>
    <div class="inner-content">
        <p><?= \Democontent2\News\Utils::replaceGallery($arResult['ITEM']['DETAIL_TEXT'], $arResult['GALLERY']) ?></p>
    </div>
    <div class="article-info">
        <div class="top">
            <div class="row">
                <div class="col-md-3 col-sm-4">
                    <? if (isset($arResult['ITEM']['AUTHOR'])): ?>
                        <div class="media user-info">
                            <? if (intval($arResult['ITEM']['AUTHOR']['PERSONAL_PHOTO'])): ?>
                                <?php
                                $avatar = CFile::ResizeImageGet(
                                    intval($arResult['ITEM']['AUTHOR']['PERSONAL_PHOTO']),
                                    array(
                                        'width' => 60,
                                        'height' => 60,
                                    ),
                                    BX_RESIZE_IMAGE_PROPORTIONAL_ALT,
                                    true
                                );
                                ?>
                                <div class="media-left">
                                    <img class="img-rounded" src="<?= SITE_TEMPLATE_PATH ?>/images/square-empty.png"
                                         alt=""
                                         style="background: url(<?= $avatar['src'] ?>) center center no-repeat; background-size: cover;">
                                </div>
                            <? endif; ?>

                            <div class="media-body">
                                <div class="media-heading">
                                    <?= $arResult['ITEM']['AUTHOR']['NAME'] ?> <?= $arResult['ITEM']['AUTHOR']['LAST_NAME'] ?>
                                </div>
                                <div class="txt"><?= Loc::getMessage('ARTICLE_AUTHOR') ?></div>
                            </div>
                            <a class="lnk-abs"
                               href="<?= SITE_DIR ?>author-<?= $arResult['ITEM']['AUTHOR']['ID'] ?>/"></a>
                        </div>
                    <? endif; ?>
                </div>
                <div class="col-sm-4 col-md-6">
                    <div class="settings text-center">
                        <span><i class="fa fa-eye"></i> <?= $arResult['VIEW_STAT'] ?></span>
                    </div>
                </div>
                <div class="col-sm-4 col-md-3">
                    <a class="text-upper scroll-top" href="#">
                        <?= Loc::getMessage('SCROLL_TOP') ?><i class="fa fa-arrow-up"></i>
                    </a>
                </div>
            </div>
        </div>
        <div class="btm">
            <div class="row">
                <div class="col-sm-6">
                    <? if (isset($arResult['ITEM']['TAGS'])): ?>
                        <div class="list-tag">
                            <?php
                            if (is_array($arResult['ITEM']['TAGS']) && count($arResult['ITEM']['TAGS']) > 0) {
                                foreach ($arResult['ITEM']['TAGS'] as $tagCode => $tagName) {
                                    echo '<a href="' . SITE_DIR . 'tag-' . $tagCode . '/">#' . $tagName . '</a>';
                                }
                            }
                            ?>
                        </div>
                    <? endif; ?>
                </div>
                <div class="col-sm-6">
                    <div class="share"><?= Loc::getMessage('SHARE_THIS') ?>
                        <div class="inner">
                            <div class="ya-share2"
                                 data-services="vkontakte,facebook,odnoklassniki,moimir,gplus,twitter,viber,whatsapp,skype,telegram"
                                 data-size="s"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="btm">
            <div class="row">
                <div class="col-sm-12">
                    <?php
                    $APPLICATION->IncludeComponent(
                        "bitrix:main.include", "",
                        array(
                            "AREA_FILE_SHOW" => "file",
                            "PATH" => SITE_TEMPLATE_PATH . "/include_areas/comments.php",
                            "EDIT_TEMPLATE" => "include_areas_template.php"
                        ),
                        false
                    );
                    ?>
                </div>
            </div>
        </div>
    </div>
</article>
<script>
    $(document).ready(function () {
        (function () {
            function numpf(n, f, s, t) {
                var n10 = n % 10;
                if ((n10 == 1) && ((n == 1) || (n > 20))) {
                    return f;
                } else if ((n10 > 1) && (n10 < 5) && ((n > 20) || (n < 10))) {
                    return s;
                } else {
                    return t;
                }
            }

            jQuery.timeago.settings.strings = {
                prefixAgo: null,
                prefixFromNow: "<?=Loc::getMessage('TIME_ACROSS')?>",
                suffixAgo: "<?=Loc::getMessage('TIME_BACK')?>",
                suffixFromNow: null,
                seconds: "<?=Loc::getMessage('TIME_LESS_MINUTE')?>",
                minute: "<?=Loc::getMessage('TIME_MINUTE_4')?>",
                minutes: function (value) {
                    return numpf(value, "%d <?=Loc::getMessage('TIME_MINUTE_1')?>", "%d <?=Loc::getMessage('TIME_MINUTE_2')?>", "%d <?=Loc::getMessage('TIME_MINUTE_3')?>");
                },
                hour: "<?=Loc::getMessage('TIME_HOUR_1')?>",
                hours: function (value) {
                    return numpf(value, "%d <?=Loc::getMessage('TIME_HOUR_1')?>", "%d <?=Loc::getMessage('TIME_HOUR_2')?>", "%d <?=Loc::getMessage('TIME_HOUR_3')?>");
                },
                day: "<?=Loc::getMessage('TIME_DAY_1')?>",
                days: function (value) {
                    return numpf(value, "%d <?=Loc::getMessage('TIME_DAY_1')?>", "%d <?=Loc::getMessage('TIME_DAY_2')?>", "%d <?=Loc::getMessage('TIME_DAY_3')?>");
                },
                month: "<?=Loc::getMessage('TIME_MONTH_1')?>",
                months: function (value) {
                    return numpf(value, "%d <?=Loc::getMessage('TIME_MONTH_1')?>", "%d <?=Loc::getMessage('TIME_MONTH_2')?>", "%d <?=Loc::getMessage('TIME_MONTH_3')?>");
                },
                year: "<?=Loc::getMessage('TIME_YEAR_1')?>",
                years: function (value) {
                    return numpf(value, "%d <?=Loc::getMessage('TIME_YEAR_1')?>", "%d <?=Loc::getMessage('TIME_YEAR_2')?>", "%d <?=Loc::getMessage('TIME_YEAR_3')?>");
                }
            };
        })();

        jQuery(".datetime").timeago();
    });
</script>
