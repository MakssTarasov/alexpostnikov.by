<?php
/**
 * Author: Ruslan Semagin
 * Email: pixel.365.24@gmail.com
 * Skype: pixel365
 * WebSite: semagin.com
 * Date: 24.01.2018
 * Time: 14:37
 */
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

$arResult['ITEMS'] = array();

use Bitrix\Main\Loader;

if (Loader::includeModule('iblock') && Loader::includeModule('highloadblock') && Loader::includeModule('democontent2.news')
) {
    $items = new \Democontent2\News\Items();
    $items->setTtl(3600);

    $arResult['ITEMS'] = $items->shortList();
}

$this->IncludeComponentTemplate();
