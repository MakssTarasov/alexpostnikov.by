<?php
/**
 * Author: Ruslan Semagin
 * Email: pixel.365.24@gmail.com
 * Skype: pixel365
 * WebSite: semagin.com
 * Date: 24.01.2018
 * Time: 19:05
 */
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

if (method_exists($this, 'setFrameMode')) {
    $this->setFrameMode(true);
}

if (!count($arResult['ITEMS'])) {
    return;
}

\Bitrix\Main\Page\Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . '/js/jquery.timeago.js');
use Bitrix\Main\Localization\Loc;

foreach ($arResult['ITEMS'] as $sectionId => $sectionValue) {
    ?>
    <div class="section-block row">
        <div class="col-md-8 col-sm-8 col-xs-12 pull-right">
            <div class="title">
                <a href="<?= SITE_DIR . $sectionValue['CODE'] ?>/"><?= $sectionValue['NAME'] ?></a>
            </div>
        </div>
        <?php
        $withPhoto = array();

        if (count($sectionValue['ITEMS']) > 1) {
            foreach ($sectionValue['ITEMS'] as $itemId => $itemValue) {
                if (intval($itemValue['PREVIEW_PICTURE']) || intval($itemValue['DETAIL_PICTURE'])) {
                    $withPhoto = $itemValue;
                    unset($sectionValue['ITEMS'][$itemId]);
                    break;
                }
            }
        }

        if (count($withPhoto) > 0) {
            $imageId = 0;

            if (intval($withPhoto['PREVIEW_PICTURE'])) {
                $imageId = intval($withPhoto['PREVIEW_PICTURE']);
            } else {
                $imageId = intval($withPhoto['DETAIL_PICTURE']);
            }

            $image = CFile::ResizeImageGet(
                $imageId,
                array(
                    'width' => 300,
                    'height' => 300
                ),
                BX_RESIZE_IMAGE_PROPORTIONAL_ALT,
                true
            );

            $arButtons = \CIBlock::GetPanelButtons(
                $withPhoto['IBLOCK_ID'],
                $withPhoto['ID'],
                0,
                array("SECTION_BUTTONS" => false, "SESSID" => false)
            );

            $arItem["EDIT_LINK"] = $arButtons["edit"]["edit_element"]["ACTION_URL"];
            $arItem["DELETE_LINK"] = $arButtons["edit"]["delete_element"]["ACTION_URL"];

            $this->AddEditAction(
                $withPhoto['ID'],
                $arItem['EDIT_LINK'],
                CIBlock::GetArrayByID(
                    $withPhoto['IBLOCK_ID'],
                    "ELEMENT_EDIT"
                )
            );
            $this->AddDeleteAction(
                $withPhoto['ID'],
                $arItem['DELETE_LINK'],
                CIBlock::GetArrayByID(
                    $withPhoto['IBLOCK_ID'],
                    "ELEMENT_DELETE"
                ),
                array(
                    "CONFIRM" => Loc::getMessage('CONFIRM_DELETE')
                )
            );
            ?>
            <div class="col-md-4 col-sm-4 col-xs-12 item-preview" id="<?= $this->GetEditAreaId($withPhoto['ID']) ?>">
                <div class="thumbnail thumbnail-news">
                    <div class="pict">
                        <div class="fotorama" data-width="100%" data-ratio="" data-height="215" data-max-width="100%"
                             data-fit="cover">
                            <img src="<?= $image['src'] ?>" alt="<?= $withPhoto['NAME'] ?>">
                        </div>
                    </div>
                    <div class="caption">
                        <div class="txt">
                            <div class="inner"><?= $withPhoto['NAME'] ?></div>
                        </div>
                    </div>
                    <a class="lnk-abs" href="<?= $withPhoto['URL'] ?>"></a>
                </div>
            </div>
        <?php
        }

        if (count($sectionValue['ITEMS']) > 0) {
            ?>
            <div class="<? if (count($withPhoto) > 0): ?>col-md-8 col-sm-8 <? endif; ?>col-xs-12">
                <div class="list">
                    <?php
                    foreach ($sectionValue['ITEMS'] as $itemId => $itemValue) {
                        $arButtons = \CIBlock::GetPanelButtons(
                            $itemValue['IBLOCK_ID'],
                            $itemValue['ID'],
                            0,
                            array("SECTION_BUTTONS" => false, "SESSID" => false)
                        );

                        $arItem["EDIT_LINK"] = $arButtons["edit"]["edit_element"]["ACTION_URL"];
                        $arItem["DELETE_LINK"] = $arButtons["edit"]["delete_element"]["ACTION_URL"];

                        $this->AddEditAction(
                            $itemValue['ID'],
                            $arItem['EDIT_LINK'],
                            CIBlock::GetArrayByID(
                                $itemValue['IBLOCK_ID'],
                                "ELEMENT_EDIT"
                            )
                        );
                        $this->AddDeleteAction(
                            $itemValue['ID'],
                            $arItem['DELETE_LINK'],
                            CIBlock::GetArrayByID(
                                $itemValue['IBLOCK_ID'],
                                "ELEMENT_DELETE"
                            ),
                            array(
                                "CONFIRM" => Loc::getMessage('CONFIRM_DELETE')
                            )
                        );
                        ?>
                        <div class="itm clearfix" id="<?= $this->GetEditAreaId($itemValue['ID']) ?>">
                            <div class="date">
                                <span class="datetime" title="<?= $itemValue['DATE_YMD_HIS'] ?>"></span>
                            </div>
                            <div class="txt"><?= $itemValue['NAME'] ?></div>
                            <a class="lnk-abs" href="<?= $itemValue['URL'] ?>"></a>
                        </div>
                    <?php
                    }
                    ?>
                </div>
            </div>
        <?php
        }
        ?>
    </div>
<?php
}
?>
<script>
    $(document).ready(function () {
        (function () {
            function numpf(n, f, s, t) {
                var n10 = n % 10;
                if ((n10 == 1) && ( (n == 1) || (n > 20) )) {
                    return f;
                } else if ((n10 > 1) && (n10 < 5) && ( (n > 20) || (n < 10) )) {
                    return s;
                } else {
                    return t;
                }
            }

            jQuery.timeago.settings.strings = {
                prefixAgo: null,
                prefixFromNow: "<?=Loc::getMessage('TIME_ACROSS')?>",
                suffixAgo: "<?=Loc::getMessage('TIME_BACK')?>",
                suffixFromNow: null,
                seconds: "<?=Loc::getMessage('TIME_LESS_MINUTE')?>",
                minute: "<?=Loc::getMessage('TIME_MINUTE_4')?>",
                minutes: function (value) {
                    return numpf(value, "%d <?=Loc::getMessage('TIME_MINUTE_1')?>", "%d <?=Loc::getMessage('TIME_MINUTE_2')?>", "%d <?=Loc::getMessage('TIME_MINUTE_3')?>");
                },
                hour: "<?=Loc::getMessage('TIME_HOUR_1')?>",
                hours: function (value) {
                    return numpf(value, "%d <?=Loc::getMessage('TIME_HOUR_1')?>", "%d <?=Loc::getMessage('TIME_HOUR_2')?>", "%d <?=Loc::getMessage('TIME_HOUR_3')?>");
                },
                day: "<?=Loc::getMessage('TIME_DAY_1')?>",
                days: function (value) {
                    return numpf(value, "%d <?=Loc::getMessage('TIME_DAY_1')?>", "%d <?=Loc::getMessage('TIME_DAY_2')?>", "%d <?=Loc::getMessage('TIME_DAY_3')?>");
                },
                month: "<?=Loc::getMessage('TIME_MONTH_1')?>",
                months: function (value) {
                    return numpf(value, "%d <?=Loc::getMessage('TIME_MONTH_1')?>", "%d <?=Loc::getMessage('TIME_MONTH_2')?>", "%d <?=Loc::getMessage('TIME_MONTH_3')?>");
                },
                year: "<?=Loc::getMessage('TIME_YEAR_1')?>",
                years: function (value) {
                    return numpf(value, "%d <?=Loc::getMessage('TIME_YEAR_1')?>", "%d <?=Loc::getMessage('TIME_YEAR_2')?>", "%d <?=Loc::getMessage('TIME_YEAR_3')?>");
                }
            };
        })();

        jQuery(".datetime").timeago();
    });
</script>
