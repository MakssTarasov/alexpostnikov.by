<?php
/**
 * Author: Ruslan Semagin
 * Email: pixel.365.24@gmail.com
 * Skype: pixel365
 * WebSite: semagin.com
 * Date: 24.01.2018
 * Time: 19:05
 */
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

if (method_exists($this, 'setFrameMode')) {
    $this->setFrameMode(true);
}
\Bitrix\Main\Page\Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . '/js/jquery.timeago.js');
use Bitrix\Main\Localization\Loc;

?>
<div class="content" style="margin-top:0;">
<div class="main-column-wrap clearfix">
<div class="col-md-3 col-sm-12 col-xs-12">
    <div class="row">
        <div class="article-preview-list">
            <?php
            $APPLICATION->IncludeComponent(
                "bitrix:main.include", "",
                array(
                    "AREA_FILE_SHOW" => "file",
                    "PATH" => SITE_TEMPLATE_PATH . "/include_areas/ad/news_line_left.php",
                    "EDIT_TEMPLATE" => "include_areas_template.php"
                ),
                false
            );

            $APPLICATION->IncludeComponent('democontent.news:last', '', array());
            ?>
        </div>
    </div>
</div>
<div class="col-md-9 col-sm-12 col-xs-12 pull-right">
<div class="section-block row">
    <div class="col-xs-12">
        <div class="list">
            <div class="itm clearfix">
                <h1><?= $arResult['META']['SECTION_PAGE_TITLE'] ?></h1>
            </div>
        </div>
    </div>
</div>
<? if (count($arResult['MOST_POPULAR']) > 0): ?>
    <div class="thumb-list thumb-list-border row">
        <?php
        foreach ($arResult['MOST_POPULAR'] as $k => $v) {
            $arButtons = \CIBlock::GetPanelButtons(
                $v['IBLOCK_ID'],
                $v['UF_ITEM_ID'],
                0,
                array("SECTION_BUTTONS" => false, "SESSID" => false)
            );

            $arItem["EDIT_LINK"] = $arButtons["edit"]["edit_element"]["ACTION_URL"];
            $arItem["DELETE_LINK"] = $arButtons["edit"]["delete_element"]["ACTION_URL"];

            $this->AddEditAction(
                $v['UF_ITEM_ID'],
                $arItem['EDIT_LINK'],
                CIBlock::GetArrayByID(
                    $v['IBLOCK_ID'],
                    "ELEMENT_EDIT"
                )
            );
            $this->AddDeleteAction(
                $v['UF_ITEM_ID'],
                $arItem['DELETE_LINK'],
                CIBlock::GetArrayByID(
                    $v['IBLOCK_ID'],
                    "ELEMENT_DELETE"
                ),
                array(
                    "CONFIRM" => Loc::getMessage('CONFIRM_DELETE')
                )
            );

            $image = CFile::ResizeImageGet(
                $v['DETAIL_PICTURE'],
                array(
                    'width' => 300,
                    'height' => 300
                ),
                BX_RESIZE_IMAGE_PROPORTIONAL_ALT,
                true
            );
            ?>
            <div class="col-md-4 col-sm-4 col-xs-12 item" id="<?= $this->GetEditAreaId($v['UF_ITEM_ID']) ?>">
                <div class="thumbnail thumbnail-news">
                    <div class="pict">
                        <div class="fotorama" data-width="100%" data-ratio="" data-height="215" data-max-width="100%" data-fit="cover">
                            <img src="<?= $image['src'] ?>" alt="<?= $v['NAME'] ?>">
                        </div>
                    </div>
                    <div class="caption">
                        <div class="txt">
                            <div class="inner">
                                <?= $v['NAME'] ?>
                            </div>
                        </div>
                    </div>
                    <a class="lnk-abs" href="<?= $v['URL'] ?>"></a>
                </div>
            </div>
        <?php
        }
        ?>
    </div>
<? endif; ?>

<?php
$APPLICATION->IncludeComponent(
    "bitrix:main.include", "",
    array(
        "AREA_FILE_SHOW" => "file",
        "PATH" => SITE_TEMPLATE_PATH . "/include_areas/list_social_buttons.php",
        "EDIT_TEMPLATE" => "include_areas_template.php"
    ),
    false
);
?>
<?php
if (count($arResult['ITEMS']) >= 6) {
    $i = 0;
    $j = 0;
    $b = 0;
    $s = 0;
    $topItemsBig = array();
    $topItemsSmall = array();

    foreach ($arResult['ITEMS'] as $itemId => $itemParams) {
        if (intval($itemParams['PREVIEW_PICTURE']) || intval($itemParams['DETAIL_PICTURE'])) {
            $b++;
            if ($b < 5) {
                $topItemsBig[$itemId] = $itemParams;
                unset($arResult['ITEMS'][$itemId]);
            } else {
                break;
            }
        }
    }

    foreach ($arResult['ITEMS'] as $itemId => $itemParams) {
        $s++;
        if ($s < 21) {
            $topItemsSmall[$itemId] = $itemParams;
            unset($arResult['ITEMS'][$itemId]);
        } else {
            break;
        }
    }

    foreach ($topItemsBig as $k => $v) {
        $imageId = 0;

        if (intval($v['PREVIEW_PICTURE'])) {
            $imageId = intval($v['PREVIEW_PICTURE']);
        } else {
            $imageId = intval($v['DETAIL_PICTURE']);
        }

        $image = CFile::ResizeImageGet(
            $imageId,
            array(
                'width' => 300,
                'height' => 300
            ),
            BX_RESIZE_IMAGE_PROPORTIONAL_ALT,
            true
        );

        $arButtons = \CIBlock::GetPanelButtons(
            $v['IBLOCK_ID'],
            $v['ID'],
            0,
            array("SECTION_BUTTONS" => false, "SESSID" => false)
        );

        $arItem["EDIT_LINK"] = $arButtons["edit"]["edit_element"]["ACTION_URL"];
        $arItem["DELETE_LINK"] = $arButtons["edit"]["delete_element"]["ACTION_URL"];

        $this->AddEditAction(
            $v['ID'],
            $arItem['EDIT_LINK'],
            CIBlock::GetArrayByID(
                $v['IBLOCK_ID'],
                "ELEMENT_EDIT"
            )
        );
        $this->AddDeleteAction(
            $v['ID'],
            $arItem['DELETE_LINK'],
            CIBlock::GetArrayByID(
                $v['IBLOCK_ID'],
                "ELEMENT_DELETE"
            ),
            array(
                "CONFIRM" => Loc::getMessage('CONFIRM_DELETE')
            )
        );
        ?>
        <div class="section-block row">
            <div class="col-md-4 col-sm-4 col-xs-12 item-preview" id="<?= $this->GetEditAreaId($v['ID']) ?>">
                <div class="thumbnail thumbnail-news">
                    <div class="pict">
                        <div class="fotorama" data-width="100%" data-ratio="" data-height="215"
                             data-max-width="100%"
                             data-fit="cover">
                            <img src="<?= $image['src'] ?>" title="<?= $v['NAME'] ?>" alt="<?= $v['NAME'] ?>">
                        </div>
                    </div>
                    <div class="caption">
                        <div class="txt">
                            <div class="inner">
                                <?= $v['NAME'] ?>
                            </div>
                        </div>
                    </div>
                    <a class="lnk-abs" href="<?= $v['URL'] ?>"></a>
                </div>
            </div>
            <div class="col-md-8 col-sm-8 col-xs-12">
                <div class="list">
                    <?php
                    $i = 0;
                    foreach ($topItemsSmall as $k_ => $v_) {
                        $i++;

                        $arButtons = \CIBlock::GetPanelButtons(
                            $v_['IBLOCK_ID'],
                            $v_['ID'],
                            0,
                            array("SECTION_BUTTONS" => false, "SESSID" => false)
                        );

                        $arItem["EDIT_LINK"] = $arButtons["edit"]["edit_element"]["ACTION_URL"];
                        $arItem["DELETE_LINK"] = $arButtons["edit"]["delete_element"]["ACTION_URL"];

                        $this->AddEditAction(
                            $v_['ID'],
                            $arItem['EDIT_LINK'],
                            CIBlock::GetArrayByID(
                                $v_['IBLOCK_ID'],
                                "ELEMENT_EDIT"
                            )
                        );
                        $this->AddDeleteAction(
                            $v_['ID'],
                            $arItem['DELETE_LINK'],
                            CIBlock::GetArrayByID(
                                $v_['IBLOCK_ID'],
                                "ELEMENT_DELETE"
                            ),
                            array(
                                "CONFIRM" => Loc::getMessage('CONFIRM_DELETE')
                            )
                        );
                        ?>
                        <div class="itm clearfix" id="<?= $this->GetEditAreaId($v_['ID']) ?>">
                            <div class="date">
                                <span class="datetime" title="<?= $v_['DATE_YMD_HIS'] ?>"></span>
                            </div>
                            <div class="txt"><?= $v_['NAME'] ?></div>
                            <a class="lnk-abs" href="<?= $v_['URL'] ?>"></a>
                        </div>
                        <?php
                        unset($topItemsSmall[$k_]);

                        if ($i % 5 == 0) {
                            break;
                        }
                    }
                    ?>
                </div>
            </div>
        </div>
    <?php
    }
}
?>
<div class="section-block row">
    <div class="col-xs-12">
        <div id="list" class="list">
            <?php
            foreach ($arResult['ITEMS'] as $itemId => $itemParams) {
                $arButtons = \CIBlock::GetPanelButtons(
                    $itemParams['IBLOCK_ID'],
                    $itemParams['ID'],
                    0,
                    array("SECTION_BUTTONS" => false, "SESSID" => false)
                );

                $arItem["EDIT_LINK"] = $arButtons["edit"]["edit_element"]["ACTION_URL"];
                $arItem["DELETE_LINK"] = $arButtons["edit"]["delete_element"]["ACTION_URL"];

                $this->AddEditAction(
                    $itemParams['ID'],
                    $arItem['EDIT_LINK'],
                    CIBlock::GetArrayByID(
                        $itemParams['IBLOCK_ID'],
                        "ELEMENT_EDIT"
                    )
                );
                $this->AddDeleteAction(
                    $itemParams['ID'],
                    $arItem['DELETE_LINK'],
                    CIBlock::GetArrayByID(
                        $itemParams['IBLOCK_ID'],
                        "ELEMENT_DELETE"
                    ),
                    array(
                        "CONFIRM" => Loc::getMessage('CONFIRM_DELETE')
                    )
                );
                ?>
                <div class="itm clearfix" id="<?= $this->GetEditAreaId($itemParams['ID']) ?>">
                    <div class="date">
                        <span class="datetime" title="<?= $itemParams['DATE_YMD_HIS'] ?>"></span>
                    </div>
                    <div class="txt"><?= $itemParams['NAME'] ?></div>
                    <a class="lnk-abs" href="<?= $itemParams['URL'] ?>"></a>
                </div>
            <?php
            }
            ?>
        </div>
    </div>
</div>
<div id="showMore">
    <? if ($arResult['ELEMENTS_FOUND'] > 10): ?>
        <div class="seporate"></div>
        <div class="text-center">
            <div class="btn btn-transparent"><?= Loc::getMessage('SHOW_MORE') ?>
                <div class="fa fa-arrow-down">&nbsp;</div>
            </div>
        </div>
    <? endif; ?>
</div>
</div>
</div>
</div>
<script>
    var path = '<?=$this->GetFolder()?>/ajax/',
        sectionCode = '<?=$arParams['sectionCode']?>',
        year = parseInt(<?=intval($arParams['year'])?>),
        month = parseInt(<?=intval($arParams['month'])?>),
        day = parseInt(<?=intval($arParams['day'])?>),
        pageTitle = '<?=Loc::getMessage('PAGE_NUM')?>',
        itemsCount = parseInt(<?=$arResult['ELEMENTS_FOUND']?>),
        totalPages = parseInt(<?=( ( ceil( ( $arResult['ELEMENTS_FOUND'] / 24 ) ) ) ? ceil( ( $arResult['ELEMENTS_FOUND'] / 24 ) ) : 1 )?>),
        page = parseInt(<?=((isset($_GET['page']) && intval($_GET['page']))?intval($_GET['page']):1)?>);

    $(document).ready(function () {
        (function () {
            function numpf(n, f, s, t) {
                var n10 = n % 10;
                if ((n10 == 1) && ( (n == 1) || (n > 20) )) {
                    return f;
                } else if ((n10 > 1) && (n10 < 5) && ( (n > 20) || (n < 10) )) {
                    return s;
                } else {
                    return t;
                }
            }

            jQuery.timeago.settings.strings = {
                prefixAgo: null,
                prefixFromNow: "<?=Loc::getMessage('TIME_ACROSS')?>",
                suffixAgo: "<?=Loc::getMessage('TIME_BACK')?>",
                suffixFromNow: null,
                seconds: "<?=Loc::getMessage('TIME_LESS_MINUTE')?>",
                minute: "<?=Loc::getMessage('TIME_MINUTE_4')?>",
                minutes: function (value) {
                    return numpf(value, "%d <?=Loc::getMessage('TIME_MINUTE_1')?>", "%d <?=Loc::getMessage('TIME_MINUTE_2')?>", "%d <?=Loc::getMessage('TIME_MINUTE_3')?>");
                },
                hour: "<?=Loc::getMessage('TIME_HOUR_1')?>",
                hours: function (value) {
                    return numpf(value, "%d <?=Loc::getMessage('TIME_HOUR_1')?>", "%d <?=Loc::getMessage('TIME_HOUR_2')?>", "%d <?=Loc::getMessage('TIME_HOUR_3')?>");
                },
                day: "<?=Loc::getMessage('TIME_DAY_1')?>",
                days: function (value) {
                    return numpf(value, "%d <?=Loc::getMessage('TIME_DAY_1')?>", "%d <?=Loc::getMessage('TIME_DAY_2')?>", "%d <?=Loc::getMessage('TIME_DAY_3')?>");
                },
                month: "<?=Loc::getMessage('TIME_MONTH_1')?>",
                months: function (value) {
                    return numpf(value, "%d <?=Loc::getMessage('TIME_MONTH_1')?>", "%d <?=Loc::getMessage('TIME_MONTH_2')?>", "%d <?=Loc::getMessage('TIME_MONTH_3')?>");
                },
                year: "<?=Loc::getMessage('TIME_YEAR_1')?>",
                years: function (value) {
                    return numpf(value, "%d <?=Loc::getMessage('TIME_YEAR_1')?>", "%d <?=Loc::getMessage('TIME_YEAR_2')?>", "%d <?=Loc::getMessage('TIME_YEAR_3')?>");
                }
            };
        })();

        jQuery(".datetime").timeago();
    });
</script>
