<?php
/**
 * Author: Ruslan Semagin
 * Email: pixel.365.24@gmail.com
 * Skype: pixel365
 * WebSite: semagin.com
 * Date: 29.01.2018
 * Time: 20:29
 */
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
$request = \Bitrix\Main\Context::getCurrent()->getRequest();

if ($request->isAjaxRequest() && $request->isPost()) {
    \Bitrix\Main\Loader::includeModule('iblock');
    \Bitrix\Main\Loader::includeModule('highloadblock');
    \Bitrix\Main\Loader::includeModule('democontent2.news');

    $params = array();

    $params['sectionCode'] = $request->get('sectionCode');

    if (intval($request->get('year'))) {
        $params['year'] = intval($request->get('year'));
    }

    if (intval($request->get('month'))) {
        $params['month'] = intval($request->get('month'));
    }

    if (intval($request->get('day'))) {
        $params['day'] = intval($request->get('day'));
    }

    $items = new \Democontent2\News\Items($params);
    $items->setPagination(
        array(
            'iNumPage' => ((intval($request->get('page'))) ? intval($request->get('page')) : 1),
            'nPageSize' => 24,
            'checkOutOfRange' => true
        )
    );

    echo \Bitrix\Main\Web\Json::encode(
        array(
            'error' => 0,
            'items' => $items->get()
        )
    );
} else {
    echo \Bitrix\Main\Web\Json::encode(
        array(
            'error' => 1
        )
    );
}
