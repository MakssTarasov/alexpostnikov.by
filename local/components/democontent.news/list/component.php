<?php
/**
 * Author: Ruslan Semagin
 * Email: pixel.365.24@gmail.com
 * Skype: pixel365
 * WebSite: semagin.com
 * Date: 24.01.2018
 * Time: 14:37
 */
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

$request = \Bitrix\Main\Context::getCurrent()->getRequest();
$items = new \Democontent2\News\Items($arParams);
$items->setPagination(
    [
        'iNumPage' => ((intval($request->get('page'))) ? intval($request->get('page')) : 1),
        'nPageSize' => 24,
        'checkOutOfRange' => true
    ]
);

$arResult['ITEMS'] = $items->get();

if (count($arResult['ITEMS']) > 0) {
    $arResult['NAV_CHAIN'] = $items->getNavChain();
    $arResult['META'] = $items->getMeta();
    $arResult['ELEMENTS_FOUND'] = $items->getSelectedRows();

    $items->setTtl(3600);
    $arResult['MOST_POPULAR'] = $items->mostPopularForAWeek($items->getSectionId());

    $this->IncludeComponentTemplate();
} else {
    CHTTP::SetStatus("404 Not Found");
    @define("ERROR_404", "Y");
    @define('NEWS_404', $this->__name);
}
