<?php
/**
 * Author: Ruslan Semagin
 * Email: pixel.365.24@gmail.com
 * Skype: pixel365
 * WebSite: semagin.com
 * Date: 01.03.2018
 * Time: 16:27
 */
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
if (\Bitrix\Main\ModuleManager::isModuleInstalled('democontent2.news')) {
    \Bitrix\Main\Loader::includeModule('iblock');

    if (\Bitrix\Main\Loader::includeModule('democontent2.news')) {
        header('Content-Type: application/json');

        $obj = new Democontent2\News\Api\Categories();
        $result = $obj->get();

        echo \Bitrix\Main\Web\Json::encode(
            array(
                'error' => ((count($result) > 0) ? 0 : 1),
                'count' => count($result),
                'result' => $result
            )
        );

        unset($obj, $result);
    } else {
        echo \Bitrix\Main\Web\Json::encode(
            array(
                'error' => 1
            )
        );
    }
} else {
    echo \Bitrix\Main\Web\Json::encode(
        array(
            'error' => 1
        )
    );
}