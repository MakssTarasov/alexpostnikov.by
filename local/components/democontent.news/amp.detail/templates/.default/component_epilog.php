<?php
/**
 * Author: Ruslan Semagin
 * Email: pixel.365.24@gmail.com
 * Skype: pixel365
 * WebSite: semagin.com
 * Date: 01.11.2017
 * Time: 11:46
 */
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

$APPLICATION->SetTitle($arResult['ITEM']['NAME']);
$APPLICATION->SetPageProperty('title', $arResult['ITEM']['NAME']);
\Bitrix\Main\Page\Asset::getInstance()->addString(
    '<link rel="canonical" href="' . SITE_DIR . $arResult['ITEM']['SECTION_CODE'] . '/' . date('Y/m/d', strtotime($arResult['ITEM']['DATE_CREATE'])) . '/' . $arResult['ITEM']['CODE'] . '/" />'
);
