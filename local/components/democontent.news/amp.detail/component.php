<?php
/**
 * Author: Ruslan Semagin
 * Email: pixel.365.24@gmail.com
 * Skype: pixel365
 * WebSite: semagin.com
 * Date: 30.01.2018
 * Time: 18:23
 */
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

$item = new \Democontent2\News\Item(array());

$arResult['ITEM'] = $item->amp(intval($arParams['id']));

$this->IncludeComponentTemplate();
