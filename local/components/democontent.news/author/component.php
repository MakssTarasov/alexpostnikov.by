<?php
/**
 * Author: Ruslan Semagin
 * Email: pixel.365.24@gmail.com
 * Skype: pixel365
 * WebSite: semagin.com
 * Date: 12.02.2018
 * Time: 11:49
 */
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

$author = new \Democontent2\News\Author(intval($arParams['id']));

$arResult['ITEM'] = $author->item();
$arResult['NAV_CHAIN'] = $author->getNavChain();

if (intval($arResult['ITEM']['AUTHOR']['ID'])) {
    $this->IncludeComponentTemplate();
} else {
    CHTTP::SetStatus("404 Not Found");
    @define("ERROR_404", "Y");
    @define('NEWS_404', $this->__name);
}
