<?php
/**
 * Author: Ruslan Semagin
 * Email: pixel.365.24@gmail.com
 * Skype: pixel365
 * WebSite: semagin.com
 * Date: 12.02.2018
 * Time: 11:50
 */
$MESS['TIME_ACROSS'] = 'через';
$MESS['TIME_BACK'] = 'назад';
$MESS['TIME_LESS_MINUTE'] = 'меньше минуты';
$MESS['TIME_MINUTE_1'] = 'минута';
$MESS['TIME_MINUTE_2'] = 'минуты';
$MESS['TIME_MINUTE_3'] = 'минут';
$MESS['TIME_MINUTE_4'] = 'минуту';
$MESS['TIME_HOUR_1'] = 'час';
$MESS['TIME_HOUR_2'] = 'часа';
$MESS['TIME_HOUR_3'] = 'часов';
$MESS['TIME_DAY_1'] = 'день';
$MESS['TIME_DAY_2'] = 'дня';
$MESS['TIME_DAY_3'] = 'дней';
$MESS['TIME_MONTH_1'] = 'месяц';
$MESS['TIME_MONTH_2'] = 'месяца';
$MESS['TIME_MONTH_3'] = 'месяцев';
$MESS['TIME_YEAR_1'] = 'год';
$MESS['TIME_YEAR_2'] = 'года';
$MESS['TIME_YEAR_3'] = 'лет';
$MESS['ARTICLE_AUTHOR'] = 'Автор статьи';
$MESS['SHARE_THIS'] = 'Поделиться:';
$MESS['SCROLL_TOP'] = 'НАВЕРХ';
$MESS['PAGE_NUM'] = 'Страница ';
$MESS['SHOW_MORE'] = 'Показать ещё';
$MESS['CONFIRM_DELETE'] = 'Вы действительно хотите удалить этот элемент?';