<?php
/**
 * Author: Ruslan Semagin
 * Email: pixel.365.24@gmail.com
 * Skype: pixel365
 * WebSite: semagin.com
 * Date: 12.02.2018
 * Time: 13:21
 */
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

$path = array();

foreach ($arResult['NAV_CHAIN'] as $navChain) {
    $path[$navChain['CODE']] = $navChain['CODE'];

    $link = implode('/', $path);

    $APPLICATION->AddChainItem($navChain['NAME'], SITE_DIR . $link . '/');
}
