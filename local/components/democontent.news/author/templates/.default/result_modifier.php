<?php
/**
 * Author: Ruslan Semagin
 * Email: pixel.365.24@gmail.com
 * Skype: pixel365
 * WebSite: semagin.com
 * Date: 02.12.2017
 * Time: 18:24
 */
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

$title = $arResult['ITEM']['AUTHOR']['NAME'] . (($arResult['ITEM']['AUTHOR']['LAST_NAME']) ? ' ' . $arResult['ITEM']['AUTHOR']['LAST_NAME'] : '');
$description = '';

$APPLICATION->SetTitle($title);
$APPLICATION->SetPageProperty("title", $title);
$APPLICATION->SetPageProperty("description", $description);
