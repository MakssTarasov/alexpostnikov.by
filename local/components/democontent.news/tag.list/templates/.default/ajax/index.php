<?php
/**
 * Author: Ruslan Semagin
 * Email: pixel.365.24@gmail.com
 * Skype: pixel365
 * WebSite: semagin.com
 * Date: 29.01.2018
 * Time: 20:29
 */
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
$request = \Bitrix\Main\Context::getCurrent()->getRequest();

if ($request->isAjaxRequest() && $request->isPost()) {
    \Bitrix\Main\Loader::includeModule('iblock');
    \Bitrix\Main\Loader::includeModule('highloadblock');
    \Bitrix\Main\Loader::includeModule('democontent2.news');

    $items = new \Democontent2\News\Items(
        array(
            'tag' => $request->get('tag')
        )
    );

    echo \Bitrix\Main\Web\Json::encode(
        array(
            'error' => 0,
            'items' => $items->getByTag(
                24,
                ((intval($request->get('page'))) ? (intval($request->get('page')) - 1) : 0)
            )
        )
    );
} else {
    echo \Bitrix\Main\Web\Json::encode(
        array(
            'error' => 1
        )
    );
}
