<?php
/**
 * Author: Ruslan Semagin
 * Email: pixel.365.24@gmail.com
 * Skype: pixel365
 * WebSite: semagin.com
 * Date: 02.12.2017
 * Time: 18:24
 */
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

$title = '';
$description = '';

foreach ($arResult['META'] as $k => $v) {
    switch ($k) {
        case 'SECTION_META_TITLE':
            $title = $v;
            break;
        case 'SECTION_META_KEYWORDS':
            $APPLICATION->SetPageProperty("keywords", $v);
            break;
        case 'SECTION_META_DESCRIPTION':
            $description = $v;
            break;
    }
}

$APPLICATION->SetTitle($title);
$APPLICATION->SetPageProperty("title", $title);
$APPLICATION->SetPageProperty("description", $description);
