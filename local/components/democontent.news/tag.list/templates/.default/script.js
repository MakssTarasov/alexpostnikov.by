var URL = {
    get: function () {
        return document.location.href;
    },
    parse: function () {
        var url = this.get();

        return url.split("?");
    }
};

function next() {
    if (page <= totalPages) {
        page = (page + 1);
    }

    var urlConstruct = '',
        _urlConstruct = '',
        f = 0,
        s = '';

    var urlParts = URL.parse(),
        params = $.parseQuery(urlParts);

    urlConstruct = '?page=' + page;

    if (page <= totalPages) {
        window.history.pushState(null, null, urlParts[0] + urlConstruct);

        $.ajax({
            type: "POST",
            url: path,
            dataType: "json",
            data: "page=" + page + "&tag=" + tag,
            beforeSend: function () {
            },
            success: function (data) {
                if (page <= totalPages) {
                    $('<div class="seporate" id="page' + page + '"></div>' +
                    '<div class="text-center">' + pageTitle + page + '</div>' +
                    '<div class="seporate"></div>').appendTo('#list');

                    var destination = $('#page' + page).offset().top;
                    $('html, body').animate({scrollTop: destination}, 800);
                }

                var put = '';

                for (key in data.items) {
                    put += '<div class="itm clearfix">';
                    put += '<div class="date"><span class="datetime" title="' + data.items[key].DATE_YMD_HIS + '"></span></div>';
                    put += '<div class="txt">' + data.items[key].NAME + '</div>';
                    put += '<a class="lnk-abs" href="' + data.items[key].URL + '"></a>';
                    put += '</div>';
                }

                $(put).appendTo('#list');

                jQuery(".datetime").timeago();

                if ((page == totalPages) || !data.count) {
                    $('#showMore').html('');
                }
            }
        });
    } else {
        $('#showMore').html('');
    }

    if (page == totalPages) {
        $('#showMore').html('');
    }
}

$(document).ready(function () {
    $('#showMore').click(
        function () {
            next();
        }
    );
});

!function (e) {
    e.parseQuery = function (r) {
        var a = {query: window.location.search || ""}, n = {};
        return "string" == typeof r && (r = {query: r}), e.extend(a, e.parseQuery, r), a.query = a.query.replace(/^\?/, ""), a.query.length > 0 && e.each(a.query.split(a.separator), function (e, r) {
            var t = r.split("="), u = "" + a.decode(t.shift(), null), o = a.decode(t.length ? t.join("=") : null, u);
            (a.array_keys.test ? a.array_keys.test(u) : a.array_keys(u)) ? (n[u] = n[u] || [], n[u].push(o)) : n[u] = o
        }), n
    }, e.parseQuery.decode = e.parseQuery.default_decode = function (e) {
        return decodeURIComponent((e || "").replace(/\+/g, " "))
    }, e.parseQuery.array_keys = function () {
        return !1
    }, e.parseQuery.separator = "&"
}(window.jQuery || window.Zepto);