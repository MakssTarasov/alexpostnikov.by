<?php
/**
 * Author: Ruslan Semagin
 * Email: pixel.365.24@gmail.com
 * Skype: pixel365
 * WebSite: semagin.com
 * Date: 24.01.2018
 * Time: 14:37
 */
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

$request = \Bitrix\Main\Context::getCurrent()->getRequest();
$items = new \Democontent2\News\Items(
    array(
        'tag' => $arParams['tag']
    )
);

$arResult['ITEMS'] = $items->getByTag(
    24,
    ((intval($request->get('page'))) ? (intval($request->get('page')) - 1) : 0)
);

if (count($arResult['ITEMS']) > 0) {
    $arResult['META'] = $items->getMeta();
    $arResult['ELEMENTS_FOUND'] = $items->getSelectedRows();

    $arResult['NAV_CHAIN'] = array(
        array(
            'NAME' => $arResult['META']['SECTION_META_TITLE'],
            'CODE' => SITE_DIR . 'tag-' . $arParams['tag'] . '/'
        )
    );

    $this->IncludeComponentTemplate();
} else {
    CHTTP::SetStatus("404 Not Found");
    @define("ERROR_404", "Y");
    @define('NEWS_404', $this->__name);
}
