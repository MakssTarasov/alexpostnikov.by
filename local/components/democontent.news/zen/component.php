<?php
/**
 * Author: Ruslan Semagin
 * Email: pixel.365.24@gmail.com
 * Skype: pixel365
 * WebSite: semagin.com
 * Date: 30.01.2018
 * Time: 12:43
 */
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Config\Option;

$items = new \Democontent2\News\Items();
$items->setTtl(3600);

$items = $items->getLast(500, true, true);

$sections = \Democontent2\News\Iblock::sections();
$domain = Option::get('democontent2.news', 'yandex_turbo_domain');

header('Content-Type: text/xml; charset=' . SITE_CHARSET, true);
echo '<?xml version="1.0" encoding="' . SITE_CHARSET . '"?>';
?>
<rss version="2.0"
     xmlns:content="http://purl.org/rss/1.0/modules/content/"
     xmlns:dc="http://purl.org/dc/elements/1.1/"
     xmlns:media="http://search.yahoo.com/mrss/"
     xmlns:atom="http://www.w3.org/2005/Atom"
     xmlns:georss="http://www.georss.org/georss">
    <channel>
        <title>
            <?php
            echo \Democontent2\News\Utils::specialCharsReplace(Option::get('democontent2.news', 'yandex_turbo_channel_name'));
            ?>
        </title>
        <link><?= $domain ?></link>
        <description>
            <?php
            echo \Democontent2\News\Utils::specialCharsReplace(Option::get('democontent2.news', 'yandex_turbo_description'));
            ?>
        </description>
        <language>ru</language>
        <?php
        foreach ($items as $k => $v) {
            $imageId = 0;

            if (intval($v['PREVIEW_PICTURE'])) {
                $imageId = intval($v['PREVIEW_PICTURE']);
            } else {
                $imageId = intval($v['DETAIL_PICTURE']);
            }
            ?>
            <item>
                <title><?= \Democontent2\News\Utils::specialCharsReplace($v['NAME']) ?></title>
                <link><?= $domain . $v['URL'] ?></link>
                <media:rating>nonadult</media:rating>
                <category><?= $v['SECTION_NAME'] ?></category>
                <?php
                if (isset($v['AUTHOR'])) {
                    echo '<author>' . \Democontent2\News\Utils::specialCharsReplace($v['AUTHOR']['NAME'] . ' ' . $v['AUTHOR']['LAST_NAME']) . '</author>';
                }
                ?>
                <category></category>
                <pubDate><?= date(DATE_RFC822, strtotime($v['DATE_CREATE'])); ?></pubDate>
                <amplink><?= $domain . SITE_DIR . 'amp/' . $v['ID'] ?>/</amplink>
                <?php
                if ($imageId) {
                    ?>
                    <enclosure url="<?= $domain . CFile::GetPath($imageId) ?>" type="image/jpeg"/>
                <?php
                }

                if (isset($v['GALLERY'])) {
                    foreach ($v['GALLERY'] as $img) {
                        ?>
                        <enclosure url="<?= $domain . CFile::GetPath($img) ?>" type="image/jpeg"/>
                    <?php
                    }
                }

                if ($v['PREVIEW_TEXT']) {
                    ?>
                    <description>
                        <![CDATA[
                        <?= \Democontent2\News\Utils::specialCharsReplace($v['PREVIEW_TEXT']) ?>
                        ]]>
                    </description>
                <?php
                }
                ?>
                <content:encoded>
                    <![CDATA[
                    <?= \Democontent2\News\Utils::specialCharsReplace($v['DETAIL_TEXT']) ?>
                    ]]>
                </content:encoded>
            </item>
        <?php
        }
        ?>
    </channel>
</rss>