<?php
/**
 * Author: Ruslan Semagin
 * Email: pixel.365.24@gmail.com
 * Skype: pixel365
 * WebSite: semagin.com
 * Date: 30.01.2018
 * Time: 18:23
 */
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc;

$currencyRates = \Democontent2\News\Utils::currencyRates(array('USD', 'EUR'));
?>
<!doctype html>
<html amp lang="<?= LANGUAGE_ID ?>">
<head>
    <meta charset="utf-8"/>
    <script async src="https://cdn.ampproject.org/v0.js"></script>
    <script async custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script>
    <?php
    if (isset($arResult['ITEM']['PROPERTIES']['GALLERY'])) {
        if (count($arResult['ITEM']['PROPERTIES']['GALLERY']) > 0) {
            ?>
            <script async custom-element="amp-carousel"
                    src="https://cdn.ampproject.org/v0/amp-carousel-0.1.js"></script>
            <?php
        }
    }

    if (\Bitrix\Main\Config\Option::get('democontent2.news', 'yandex_turbo_google_analytics_id')) {
        ?>
        <script async custom-element="amp-analytics" src="https://cdn.ampproject.org/v0/amp-analytics-0.1.js"></script>
        <?php
    }
    ?>
    <title><? $APPLICATION->ShowTitle() ?></title>
    <meta name="viewport" content="width=device-width,minimum-scale=1,initial-scale=1">
    <style amp-boilerplate>body {
            -webkit-animation: -amp-start 8s steps(1, end) 0s 1 normal both;
            -moz-animation: -amp-start 8s steps(1, end) 0s 1 normal both;
            -ms-animation: -amp-start 8s steps(1, end) 0s 1 normal both;
            animation: -amp-start 8s steps(1, end) 0s 1 normal both
        }

        @-webkit-keyframes -amp-start {
            from {
                visibility: hidden
            }
            to {
                visibility: visible
            }
        }

        @-moz-keyframes -amp-start {
            from {
                visibility: hidden
            }
            to {
                visibility: visible
            }
        }

        @-ms-keyframes -amp-start {
            from {
                visibility: hidden
            }
            to {
                visibility: visible
            }
        }

        @-o-keyframes -amp-start {
            from {
                visibility: hidden
            }
            to {
                visibility: visible
            }
        }

        @keyframes -amp-start {
            from {
                visibility: hidden
            }
            to {
                visibility: visible
            }
        }</style>
    <noscript>
        <style amp-boilerplate>body {
                -webkit-animation: none;
                -moz-animation: none;
                -ms-animation: none;
                animation: none
            }</style>
    </noscript>
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto">
    <?php
    $APPLICATION->ShowHeadStrings();
    ?>
    <style amp-custom>
        body {
            width: auto;
            margin: 0;
            padding: 0;
            font-family: 'Roboto', sans-serif;
            font-size: 0.9em;
        }

        header {
            background: #ffffff;
            color: #999;
            text-align: center;
            border-bottom: 1px solid #f8f8f8;
        }

        h1 {
            margin: 0;
            padding: 0.5em;
            background: white;
            color: #2d2d2d;
            box-shadow: 0px 2px 4px #ececec;
        }

        p {
            padding: 0.5em;
            margin: 0.5em;
        }

        .sidebar {
            width: 100%;
            padding: 0;
            margin: 0;
        }

        .sidebar > li {
            width: 100%;
            height: 40px;
            line-height: 40px;
            list-style: none;
            padding-left: 15px;
            color: #4c4c4c;
            text-align: left;
            border-bottom: 1px solid #e6e6e6;
        }

        .sidebar > li a {
            width: 100%;
            height: 40px;
            line-height: 40px;
            text-decoration: none;
            color: #999;
            display: inline-block;
        }

        .close-sidebar {
            font-size: 1.5em;
            padding-left: 5px;
        }

        .sidebar-bg {
            width: 50%;
            background: #ffffff;
        }

        header .header-top {
            padding: 0 35px;
            border-bottom: 1px solid #f8f8f8;
            z-index: 3;
        }

        header, header .header-top {
            background: #fff;
            position: relative;
        }

        .weather {
            float: left;
            height: 50px;
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
            flex-flow: row wrap;
            -webkit-box-align: center;
            -ms-flex-align: center;
            align-items: center;
            -webkit-box-pack: center;
            -ms-flex-pack: center;
            justify-content: center;
            color: 666666;
        }

        .city-box .head, .weather {
            -webkit-box-orient: horizontal;
            -webkit-box-direction: normal;
            -ms-flex-flow: row wrap;
        }

        header .header-top > .inner {
            height: 90px;
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
            flex-flow: row wrap;
            -webkit-box-align: center;
            -ms-flex-align: center;
            align-items: center;
            -webkit-box-pack: center;
            -ms-flex-pack: center;
            justify-content: center;
            margin: 0 180px;
        }

        header .logo amp-img {
            width: 103px;
            height: 18px;
        }

        .sub-header {
            height: 50px;
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
            flex-flow: row wrap;
            -webkit-box-align: center;
            -ms-flex-align: center;
            align-items: center;
            -webkit-box-pack: center;
            -ms-flex-pack: center;
            justify-content: center;
            margin: 0 180px;
        }

        header .sub-header .burger {
            cursor: pointer;
            margin-right: 15px;
            position: relative;
            position: absolute;
            left: 0;
            bottom: 0;
            background: #fff;
            width: 50px;
            height: 50px;
            overflow: hidden;
            border-right: 1px solid #f8f8f8;
        }

        header .sub-header .burger > span {
            position: absolute;
            width: 100%;
            height: 100%;
            top: 0;
            left: 0;
            -webkit-transition: all .3s ease 0s;
            transition: all .3s ease 0s;
        }

        header .sub-header .burger > span.open span {
            width: 25px;
            height: 3px;
            position: absolute;
            left: 50%;
            margin-left: -12.5px;
            top: 50%;
            margin-top: -1.5px;
            background: #666;
        }

        header .sub-header .burger > span.open span:before {
            top: -7px;
        }

        header .sub-header .burger > span.open span:after {
            bottom: -7px;
        }

        header .sub-header .burger > span.open span:after, header .sub-header .burger > span.open span:before {
            content: '';
            width: 100%;
            height: 100%;
            position: absolute;
            left: 0;
            background: inherit;
        }

        .date-create {
            height: 30px;
            margin-top: 5px;
            background: #ffffff;
            color: #999;
            text-align: center;
            border-bottom: 1px solid #f8f8f8;
        }

        .date-create > .inner {
            height: 30px;
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
            flex-flow: row wrap;
            align-items: center;
            padding-left: 10px;
            padding-right: 10px;
            font-size: 0.8em;
        }

        footer {
            margin-top: 30px;
            padding: 10px;
            font-weight: 500;
            color: grey;
        }

        footer, footer > .container {
            background: #212121;
        }

        .arr-red {
            color: #ff0000;
        }

        .arr-green {
            color: #3f9f16;
        }
    </style>
</head>
<body>
<header>
    <div class="header-top">
        <div class="inner">
            <a href="<?= SITE_DIR ?>">
                <amp-img src="<?= SITE_TEMPLATE_PATH ?>/images/logo.png" width="103" height="18"></amp-img>
            </a>
        </div>
    </div>
    <div class="sub-header">
        <div class="burger" role="button" on="tap:sidebar1.toggle" tabindex><span class="open"><span></span></span>
        </div>
        <div class="weather">
            <div class="inner">
                <?= date('d.m.Y H:i') ?>
                <?php
                if (count($currencyRates['TODAY']) > 0) {
                    foreach ($currencyRates['TODAY'] as $k => $v) {
                        $arr = '';

                        if (count($currencyRates['YESTERDAY']) > 0) {
                            if (isset($currencyRates['YESTERDAY'][$k])) {
                                if (round($v, 2) > round($currencyRates['YESTERDAY'][$k], 2)) {
                                    $arr = '<span class="arr-red">	&#9650;</span>';
                                }

                                if (round($v, 2) < round($currencyRates['YESTERDAY'][$k], 2)) {
                                    $arr = '<span class="arr-green">&#9660;</span>';
                                }
                            }
                        }

                        echo ' ' . $k . ' ' . round($v, 2) . $arr;
                    }
                }
                ?>
            </div>
        </div>
    </div>
</header>
<amp-sidebar class="sidebar-bg" id="sidebar1" layout="nodisplay" side="left">
    <div role="button" aria-label="close sidebar" on="tap:sidebar1.toggle" tabindex="0" class="close-sidebar">x</div>
    <ul class="sidebar">
        <?php
        $APPLICATION->IncludeComponent(
            "bitrix:main.include", "",
            array(
                "AREA_FILE_SHOW" => "file",
                "PATH" => SITE_TEMPLATE_PATH . "/include_areas/menu/left.php",
                "EDIT_TEMPLATE" => "include_areas_template.php",
                'MODE' => 'php'
            ),
            false
        );
        ?>
    </ul>
</amp-sidebar>
<article>
    <h1><?= $arResult['ITEM']['NAME'] ?></h1>

    <div class="date-create">
        <div class="inner">
            <?= date('d.m.Y H:i', strtotime($arResult['ITEM']['DATE_CREATE'])) ?>,
            <?= $arResult['ITEM']['AUTHOR']['NAME'] ?> <?= $arResult['ITEM']['AUTHOR']['LAST_NAME'] ?>
        </div>
    </div>
    <p><?= $arResult['ITEM']['PREVIEW_TEXT'] ?></p>
    <?php
    $imageId = 0;

    if (intval($arResult['ITEM']['PREVIEW_PICTURE'])) {
        $imageId = intval($arResult['ITEM']['PREVIEW_PICTURE']);
    } else {
        $imageId = intval($arResult['ITEM']['DETAIL_PICTURE']);
    }

    if ($imageId) {
        $image = CFile::ResizeImageGet(
            $imageId,
            array(
                'width' => 600,
                'height' => 600
            ),
            BX_RESIZE_IMAGE_PROPORTIONAL_ALT,
            true
        );

        echo '<amp-img src="' . $image['src'] . '" layout="responsive" width="' . $image['width'] . '" height="' . $image['height'] . '"></amp-img>';
    }
    ?>
    <p><?= $arResult['ITEM']['DETAIL_TEXT'] ?></p>
    <?php
    if (isset($arResult['ITEM']['PROPERTIES']['GALLERY'])) {
        if (count($arResult['ITEM']['PROPERTIES']['GALLERY']) > 0) {
            $gallery = array();

            foreach ($arResult['ITEM']['PROPERTIES']['GALLERY'] as $img) {
                $image = CFile::ResizeImageGet(
                    intval($img['VALUE']),
                    array(
                        'width' => 400,
                        'height' => 300
                    ),
                    BX_RESIZE_IMAGE_PROPORTIONAL_ALT,
                    true
                );

                if (isset($image['src'])) {
                    $gallery[] = '<amp-img src="' . $image['src'] . '" width="400" height="300" alt="' . $arResult['ITEM']['NAME'] . '"></amp-img>';
                }
            }

            if (count($gallery)) {
                ?>
                <amp-carousel height="300" layout="fixed-height" type="carousel" loop>
                    <?php
                    echo implode('', $gallery);
                    ?>
                </amp-carousel>
                <?php
            }
        }
    }
    ?>
</article>
<footer>
    <?php
    $APPLICATION->IncludeComponent(
        "bitrix:main.include", "",
        array(
            "AREA_FILE_SHOW" => "file",
            "PATH" => SITE_TEMPLATE_PATH . "/include_areas/footer.php",
            "EDIT_TEMPLATE" => "include_areas_template.php",
            'MODE' => 'php'
        ),
        false
    );
    ?>
</footer>
<?php
if (\Bitrix\Main\Config\Option::get('democontent2.news', 'yandex_turbo_google_analytics_id')) {
    ?>
    <amp-analytics type="googleanalytics">
        <script type="application/json">
            {
                "vars": {
                    "account": "<?= \Bitrix\Main\Config\Option::get('democontent2.news', 'yandex_turbo_google_analytics_id') ?>"
                },
                "triggers": {
                    "default pageview": {
                        "on": "visible",
                        "request": "pageview",
                        "vars": {
                            "title": "<?= $arResult['ITEM']['NAME'] ?>"
                        }
                    }
                }
            }



        </script>
    </amp-analytics>
    <?php
}
?>
</body>
</html>
