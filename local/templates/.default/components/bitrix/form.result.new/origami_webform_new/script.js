function sendForm(sid, color)
{
	if ($("form[name='" + sid + "'] input[name='personal']").is(':checked'))
	{
		$("form[name='" + sid + "'] input[type='submit']").trigger('click');
	}
	else
	{
		$('.feedback_block__compliance svg path').css({'stroke': color, 'stroke-dashoffset': 0});
	}
}

function fixCountryPopup(element) {
    let timer = 3000;

    let waitElement = setInterval(function () {
        if (document.getElementById('phoneNumberInputSelectCountry')) {
            setFixCountryPopupPosition(element);
            clearInterval(waitElement);
        }

        timer -= 0.3;

        if (timer <= 0) {
            clearInterval(waitElement);
        }
    }, 0.3);
}

function setFixCountryPopupPosition(element) {
    let top = element.getBoundingClientRect().top;
    document.getElementById('phoneNumberInputSelectCountry').style.top = top - 200 + 'px';
    document.getElementById('phoneNumberInputSelectCountry').style.opacity = '1';
}
