<?

use Bitrix\Main\Page\Asset;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

Asset::getInstance()->addCss(SITE_DIR . "local/templates/.default/components/bitrix/form.result.new/origami_webform_2/style.css");
$prefix = \Bitrix\Main\Security\Random::getString(5);
$telMask = \Sotbit\Origami\Config\Option::get('MASK', SITE_ID);
?>

<div class="puzzle_block-form-promotion feedback">
    <div class="feedback_block__text">
        <? if ($arResult["isFormTitle"] == "Y"): ?>
            <div class="feedback_block__title fonts__middle_title"><?= $arResult["FORM_TITLE"] ?></div>
        <? endif ?>
        <? if ($arResult["isFormDescription"] == "Y"): ?>
            <div class="feedback_block__comment fonts__small_text"><?= $arResult["FORM_DESCRIPTION"] ?></div>
        <? endif ?>
    </div>

    <?= $arResult["FORM_HEADER"] ?>
    <div class="form_blocks">
        <? if ($_GET['formresult'] == 'addok') {
            ?>
            <div class="form_blocks__ok"><?= GetMessage("OK_MESSAGE") ?></div>
            <?
        }
        ?>
        <div class="row">
            <div class="col-md-6 form-block-left">
                <div class="form_block_title">
                    <?= GetMessage('SOTBIT_FORM_TITLE_1'); ?>
                </div>
                <?
                foreach ($arResult["QUESTIONS"] as $FIELD_SID => $arQuestion) {
                    $fieldType = $arQuestion['STRUCTURE'][0]['FIELD_TYPE'];
                    if ($fieldType == 'textarea') {
                        continue;
                    }
                    if ($fieldType == 'hidden') {
                        echo $arQuestion["HTML_CODE"];
                    } else {
                        ?>
                        <div class="form_row">
                            <label class="puzzle_block__form_label_promotion"
                                   for="<?= $arQuestion['STRUCTURE'][0]['ID'] ?>">
                                <?= $arQuestion["CAPTION"] ?> <?= ($arQuestion['REQUIRED'] == 'Y') ? '<span class="required">*</span>' : '' ?>
                            </label>
                            <input id="<?= $arQuestion['STRUCTURE'][0]['ID'] ?>"
                                   type="<?= $arQuestion['STRUCTURE'][0]['FIELD_TYPE'] ?>"
                                   class="feedback_block__form_input <?= ($arQuestion['STRUCTURE'][0]['FIELD_TYPE'] == 'tel' ? "js-phone" : "") ?>"
                                   name="form_<?= ($arQuestion['STRUCTURE'][0]['FIELD_TYPE'] == 'tel') ? 'text' : $arQuestion['STRUCTURE'][0]['FIELD_TYPE'] ?>_<?= $arQuestion['STRUCTURE'][0]['ID'] ?>"
                                <? if ($arQuestion['STRUCTURE'][0]['FIELD_TYPE'] == 'tel'): ?>
                                    <?= ($arQuestion['STRUCTURE'][0]['MASK']) ? 'placeholder="' . $arQuestion['STRUCTURE'][0]['MASK'] . '"' : "" ?>
                                    <?= ($arQuestion['STRUCTURE'][0]['PATTERN']) ? 'pattern="' . $arQuestion['STRUCTURE'][0]['PATTERN'] . '"' : '' ?>
                                <? endif; ?>
                            >
                        </div>
                        <?
                    }
                }
                ?>
            </div>
            <div class="col-md-6 form-block-right">
                <?
                foreach ($arResult["QUESTIONS"] as $FIELD_SID => $arQuestion) {
                    $fieldType = $arQuestion['STRUCTURE'][0]['FIELD_TYPE'];
                    if ($fieldType != 'textarea') {
                        continue;
                    }
                    if ($fieldType == 'hidden') {
                        echo $arQuestion["HTML_CODE"];
                    } else {
                        ?>
                        <label class="puzzle_block__form_label_promotion form_block_title" for="">
                            <?= $arQuestion["CAPTION"] ?> <?= ($arQuestion['REQUIRED'] == 'Y') ? '<span class="required">*</span>' : '(' . GetMessage('SOTBIT_NOT_REQUIRED') . ')' ?>
                        </label>
                        <textarea class="feedback_block__form_input"
                                  name="form_<?= $fieldType ?>_<?= $arQuestion['STRUCTURE'][0]['ID'] ?>"
                                  <?= ($arQuestion['REQUIRED'] == 'Y') ? '' : '' ?>></textarea>
                        <?
                    }
                }
                ?>
            </div>
        </div>
        <?
        if ($arResult["isUseCaptcha"] == "Y") {
            ?>
            <div class="feedback_block__captcha">
                <div class="captcha_form-2">
                    <p class="puzzle_block__form_label_promotion captcha_title">
                        <?= GetMessage('CAPTCHA_TITLE') ?>
                    </p>
                    <div class="form_row captcha">
                        <div class="feedback_block__captcha_input">
                            <input type="text" name="captcha_word" size="30" maxlength="50" value="" required
                                   class="feedback_block__form_input "/>
                        </div>
                        <div class="feedback_block__captcha_img">
                            <input type="hidden" name="captcha_sid"
                                   value="<?= htmlspecialcharsbx($arResult["CAPTCHACode"]); ?>"/>
                            <img
                                src="/bitrix/tools/captcha.php?captcha_sid=<?= htmlspecialcharsbx($arResult["CAPTCHACode"]); ?>"
                                width="180" height="40"/>
                            <div class="captcha-refresh" onclick="reloadCaptcha(this,'<?= SITE_DIR ?>');return false;">
                                <svg class="icon_refresh" width="16" height="14">
                                    <use
                                        xlink:href="/local/templates/sotbit_origami/assets/img/sprite.svg#icon_refresh"></use>
                                </svg>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?
        }
        ?>
        <div class="feedback_block__compliance">
            <div class="feedback_block__compliance main_checkbox">
                <input type="checkbox" id="personal_phone_personal<?= $prefix ?>" class="checkbox__input"
                       checked="checked" name="personal">
                <label for="personal_phone_personal<?= $prefix ?>">
                    <span></span>
                    <span> <?= GetMessage('FORM_CONFIDENTIAL_1') ?>
                             <a class="feedback_block__compliance_link"
                                href="<?= \Sotbit\Origami\Helper\Config::get('CONFIDENTIAL_PAGE') ?>"><?= GetMessage('FORM_CONFIDENTIAL_2') ?>
                            </a>
                        </span>
                </label>
            </div>
        </div>
        <div class="feedback_block__input_wrapper">
            <input
                type="button"
                class="feedback_block__input main_btn button_feedback sweep-to-right"
                name="web_form_submit"
                value="<?= GetMessage("FORM_SUBMIT") ?>"
                onclick="sendForm('<?= $arResult['arForm']['SID'] ?>','<?= \Sotbit\Origami\Helper\Config::get('COLOR_BASE') ?>')"
            >
            <input style="display: none" type="reset" class="feedback_block__input main_btn
					button_feedback sweep-to-right" name="web_form_submit"
                   value="<?= GetMessage("FORM_RESET") ?>">
            <input type="submit" style="display:none"
                   name="web_form_submit" id="submit">
        </div>
    </div>
</div>
<?= $arResult["FORM_FOOTER"] ?>
</div>

<script>
    $(document).ready(function () {
        $('.js-phone').inputmask("<?= $telMask ?>");
    });
</script>
