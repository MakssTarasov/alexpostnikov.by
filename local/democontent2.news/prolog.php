<?php
/**
 * Author: Ruslan Semagin
 * Email: pixel.365.24@gmail.com
 * Skype: pixel365
 * WebSite: semagin.com
 * Date: 29.06.2016
 * Time: 10:26
 */

require( $_SERVER[ "DOCUMENT_ROOT" ] . "/bitrix/modules/main/include/prolog_before.php" );
include_once( $_SERVER[ 'DOCUMENT_ROOT' ] . '/urlrewrite.php' );

use Bitrix\Main\Loader;
use Democontent2\News\Router;

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, OPTIONS');
header('Access-Control-Allow-Headers: Content-Type');
//header('Content-Type: application/json');

Loader::includeModule( 'iblock' );
Loader::includeModule( 'highloadblock' );
Loader::includeModule( 'democontent2.news' );

$router = new Router($_GET);
$includeComponent = $router->getMap();

if( !empty( $includeComponent ) && count( $includeComponent ) > 0 ) {
    $APPLICATION->IncludeComponent(
        ( ( isset( $includeComponent[ 'namespace' ] ) ) ? $includeComponent[ 'namespace' ] : 'democontent.news' ) . ":" . $includeComponent[ 'component' ],
        ( ( isset( $includeComponent[ 'template' ] ) ) ? $includeComponent[ 'template' ] : '.default' ),
        ( ( isset( $includeComponent[ 'params' ] ) && !empty( $includeComponent[ 'params' ] ) ) ? $includeComponent[ 'params' ] : array() )
    );
}