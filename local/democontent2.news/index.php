<?php
/**
 * Author: Ruslan Semagin
 * Email: pixel.365.24@gmail.com
 * Skype: pixel365
 * WebSite: semagin.com
 * Date: 24.01.2018
 * Time: 14:29
 */
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
include_once($_SERVER['DOCUMENT_ROOT'] . '/urlrewrite.php');

\Bitrix\Main\Loader::includeModule('iblock');
\Bitrix\Main\Loader::includeModule('highloadblock');
\Bitrix\Main\Loader::includeModule('democontent2.news');

$router = new \Democontent2\News\Router($_GET);
$includeComponent = $router->getMap();

if (!empty($includeComponent) && count($includeComponent) > 0) {
    $APPLICATION->IncludeComponent(
        ((isset($includeComponent['namespace'])) ? $includeComponent['namespace'] : 'democontent.news') . ":" . $includeComponent['component'],
        ((isset($includeComponent['template'])) ? $includeComponent['template'] : '.default'),
        ((isset($includeComponent['params']) && !empty($includeComponent['params'])) ? $includeComponent['params'] : array())
    );
} else {
    //$APPLICATION->IncludeComponent( "democontent:realty.404", '.default', array() );
}

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php");