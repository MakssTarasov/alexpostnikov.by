<?php
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Новостной портал");
$APPLICATION->SetPageProperty("title", "Новостной портал");
$APPLICATION->SetPageProperty("description", "Актуальные новости в стране и мире. Политика, спорт, проишествия, прогноз погоды, курсы валют");
$APPLICATION->SetPageProperty("NOT_SHOW_NAV_CHAIN", "Y");
?>
<div class="content">
    <div class="thumb-list clearfix">
        <?php
        $APPLICATION->IncludeComponent('democontent.news:main', '', array());
        ?>
    </div>
    <div class="main-column-wrap clearfix">
        <div class="col-md-3 col-sm-12 col-xs-12">
            <div class="row">
                <div class="article-preview-list">
                    <?php
                    $APPLICATION->IncludeComponent(
                        "bitrix:main.include", "",
                        array(
                            "AREA_FILE_SHOW" => "file",
                            "PATH" => SITE_TEMPLATE_PATH . "/include_areas/ad/news_line_left.php",
                            "EDIT_TEMPLATE" => "include_areas_template.php"
                        ),
                        false
                    );

                    $APPLICATION->IncludeComponent('democontent.news:last', '', array());
                    ?>
                </div>
            </div>
        </div>
        <div class="col-md-9 col-sm-12 col-xs-12 pull-right">
            <?php
            $APPLICATION->IncludeComponent(
                "bitrix:main.include", "",
                array(
                    "AREA_FILE_SHOW" => "file",
                    "PATH" => SITE_TEMPLATE_PATH . "/include_areas/list_social_buttons.php",
                    "EDIT_TEMPLATE" => "include_areas_template.php"
                ),
                false
            );

            $APPLICATION->IncludeComponent('democontent.news:short.list', '', array());
            ?>
        </div>
    </div>
</div>
<?php
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php");
?>
